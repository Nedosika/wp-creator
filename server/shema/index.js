import {gql} from "apollo-server-express";

import taskQueue, {TASKS_UPDATED, pubSub} from "../queues/task.js";
import {getPostData} from "../helpers/post.js";
import {makeExecutableSchema} from "@graphql-tools/schema";

const typeDefs = gql`
  scalar Date
  type Url {
    key: String
    url: String
    webArchiveUrl: String
    date: Date
  }
  input UrlInput {
    key: String
    url: String
    webArchiveUrl: String
    date: Date
  }
  input CheckInput {
    contentSelector: String
    contentReplacers: [String]
    dateSelector: String
    dateParser: String
    dateLocale: String
    onlyHtml: Boolean
    headerSelector: String
    url: UrlInput
  }
  input TaskInput {
    contentSelector: String
    contentReplacers: [String]
    dateSelector: String
    dateParser: String
    dateLocale: String
    username: String
    password: String
    description: String
    isAddCategories: Boolean
    isStrongSearch: Boolean
    name: String
    onlyHtml: Boolean
    order: String
    sitemap: String
    sortBy: String
    status: String
    headerSelector: String
    timeout: Int
    endpoint: String
    urls: [UrlInput]
  }
  type Check {
    slug: String
    slugCategories: [String]
    header: String
    title: String
    date: Date
    content: String
    description: String
  }
  type Task {
    id: ID
    contentSelector: String
    contentReplacers: [String]
    dateSelector: String
    dateParser: String
    dateLocale: String
    username: String
    password: String
    description: String
    isAddCategories: Boolean
    isStrongSearch: Boolean
    name: String
    onlyHtml: Boolean
    order: String
    sitemap: String
    sortBy: String
    status: String
    headerSelector: String
    timeout: Int
    endpoint: String
    urls: [Url]
    progress: Int
  }
  type Query {
    hello: String
    tasks: [Task]
    task(id: ID): Task
    check(data: CheckInput): Check
  }
  type Mutation {
    createTask(data: TaskInput): ID
    deleteTask(id: ID): ID
    removeTasks(ids: [ID]): [ID]
    updateTask(id: ID, task: TaskInput): Task
  }
  type Subscription {
    tasksUpdated: Task
  }
`;

const resolvers = {
    Query: {
        tasks: async () => {
            const tasks = await taskQueue.getJobs([
                "active",
                "waiting",
                "completed",
                "failed",
            ]);
            return tasks.map(({id, data, _progress, status}) => ({
                id,
                ...data,
                progress: _progress,
                status,
            }));
        },
        task: async (_, args) => {
            const result = await taskQueue.getJob(args.id);
            return {id: result.id, ...result.data};
        },
        check: (_, {data: {url, ...data}}) =>
            getPostData({
                ...url,
                ...data,
            }),
    },
    Mutation: {
        createTask: async (_, {data}) => {
            console.log({data});
            const {id} = await taskQueue.add(data);
            await pubSub.publish(TASKS_UPDATED, {tasksUpdated: {id, ...data}});
            return id;
        },
        deleteTask: async (_, {id}) => {
            const job = await taskQueue.getJob(id);

            if (!job) {
                throw new Error(`Task with ID ${id} not found`);
            }

            await job.remove();

            await pubSub.publish(TASKS_UPDATED, {tasksUpdated: {id}});

            return id;
        },
        removeTasks: (_, {ids}) =>
            Promise.all(
                ids.map(async (id) => {
                    const job = await taskQueue.getJob(id);
                    await job.remove();
                    await pubSub.publish(TASKS_UPDATED, {tasksUpdated: {id}});
                    return id;
                }),
            ),
    },
    Subscription: {
        tasksUpdated: {
            subscribe: () => pubSub.asyncIterator([TASKS_UPDATED]),
        },
    },
};

export const schema = makeExecutableSchema({typeDefs, resolvers});
