import Queue from "bull";
import WpAPI from "wpapi";
import {PubSub} from "graphql-subscriptions";
import {getPostData} from "../helpers/post.js";

export const pubSub = new PubSub();
export const TASKS_UPDATED = "TASKS_UPDATED";
const taskQueue = new Queue("myJobQueue");
const delay = (duration) =>
    new Promise((resolve) => setTimeout(resolve, duration));

taskQueue.process(async (job, done) => {
    const {
        data: {
            endpoint,
            password,
            timeout,
            urls,
            username,
            ...settings
        },
    } = job;
    const wp = new WpAPI({endpoint, username, password});
    const countUrls = urls.length;

    urls
        .reduce(
            (promise, currentUrl, index, arr) =>
                promise.then(async (prev) => {
                    job.progress(Math.ceil(((index + 1) * 100) / countUrls)).catch((err) => console.log(err));

                    await delay(timeout);

                    try {
                        const {
                            content,
                            date,
                            description,
                            header,
                            slug,
                            slugCategories,
                            title,
                        } = await getPostData({
                            ...currentUrl,
                            ...settings
                        });

                        if(!content)
                            return console.log("Content not found!")

                        const category = await slugCategories.reduce(
                                (promise, categoryCandidate) =>
                                    promise.then(
                                        async (parent = 0) => {
                                            const [category] = await wp.categories().slug(categoryCandidate);

                                            if (category)
                                                return category.id;

                                            const {id} = await wp.categories().create({
                                                name: categoryCandidate,
                                                slug: categoryCandidate,
                                                parent
                                            });

                                            return id;
                                        },
                                    ).catch((error) => console.log(error.message)),
                                Promise.resolve(),
                            );

                        await wp.posts().create({
                                title: header,
                                slug,
                                categories: category ? [category] : [],
                                content,
                                yoast_meta: {
                                    yoast_wpseo_title: title,
                                    yoast_wpseo_metadesc: description,
                                },
                                date,
                                status: "publish",
                            });
                    } catch (e) {
                        console.log(e.message);
                    }
                }),
            Promise.resolve(),
        )
        .finally(() => {
            console.log("done");
            done();
        });
});

taskQueue.on("progress", ({data}, progress) => {
    console.log({data});
    pubSub.publish(TASKS_UPDATED, {tasksUpdated: {...data, progress}});
});

export default taskQueue;
