import {trimChar} from "./trimChar.js";

export const getSlug = (url) => {
    const partedUrl = trimChar(url, '/').split('/');
    return {
        domain: `${partedUrl[2].split(":")[0]}/`.replace("www.", ""),
        slug: partedUrl[partedUrl.length - 1].replace('.html', '').replace('.htm', '')
    };
}