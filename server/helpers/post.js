import {JSDOM} from "jsdom";
import {getSlug} from "./slug.js";
import {getCategories} from "./categories.js";
import moment from "moment";

const updateElements = (elements, domain, type) =>
    elements.forEach((element) => {
        const current = element.getAttribute(type);
        if(!current)
            return;
        const newLink = current
            .substring(current.indexOf(domain))
            .replace(domain, '')
        element.setAttribute(type, newLink && newLink[0] === '/' ? newLink : `/${newLink}`);
    });
export const getPostData = async ({
                                      url,
                                      webArchiveUrl,
                                      date: webArchiveDate,
                                      dateSelector,
                                      dateParser,
                                      dateLocale,
                                      headerSelector,
                                      contentReplacers,
                                      contentSelector,
                                  }) => {
    console.log({
        url,
        webArchiveUrl,
        webArchiveDate,
        dateSelector,
        dateParser,
        dateLocale,
        headerSelector,
        contentReplacers,
        contentSelector
    });

    const {
        window: {document, close},
    } = await JSDOM.fromURL(`${webArchiveUrl}/${url}`);

    const {domain, slug} = getSlug(url);
    const slugCategories = getCategories(url);
    const header = headerSelector
        ? document.querySelector(headerSelector)?.textContent
        : "";
    const title = document.querySelector("title")?.textContent;

    const dateFromSelector = dateSelector && document.querySelector(dateSelector)?.textContent;

    const date = dateFromSelector
        ? moment(dateFromSelector, dateParser || "", dateLocale || "ru").toDate()
        : document.querySelector('meta[property="article:published_time"]')?.getAttribute("content") || new Date(webArchiveDate);

    const description = document.querySelector('meta[name="description"]')?.getAttribute("content") || "";

    const imgElements = document.querySelectorAll("img");
    updateElements(imgElements, domain, 'src');
    updateElements(imgElements, `${webArchiveUrl}`, 'src');

    const anchorElements = document.querySelectorAll('a');
    updateElements(anchorElements, domain, 'href');
    updateElements(anchorElements, `${webArchiveUrl}`, 'href');

    const scriptElements = document.querySelectorAll("script");
    scriptElements.forEach((script) => script.remove());

    const content = contentReplacers.reduce(
        (result, replacer) => result?.replace(new RegExp(replacer, "g"), ""),
        document.querySelector(contentSelector)?.innerHTML,
    );

    console.log({
        content,
        date,
        description,
        domain,
        header,
        slug,
        slugCategories,
        title,
    })

    return {
        content,
        date,
        description,
        header,
        slug,
        slugCategories,
        title,
    };
};
