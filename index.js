import express from "express";
import {createServer} from "http";
import path from "path";
import {ApolloServer} from "apollo-server-express";
import cookieParser from "cookie-parser";
import bodyParser from "body-parser";
import {fileURLToPath} from "url";
import {ApolloServerPluginDrainHttpServer} from "@apollo/server/plugin/drainHttpServer";

import CONFIG from "./config.js";
import {schema} from "./server/shema/index.js";
import {WebSocketServer} from "ws";
import {useServer} from "graphql-ws/lib/use/ws";

const port = CONFIG.PORT || 3000;
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const expressServer = express();
const httpServer = createServer(expressServer);
const wsServer = new WebSocketServer({
    server: httpServer,
    path: "/graphql",
});
const serverCleanup = useServer({schema}, wsServer);

const apolloServer = new ApolloServer({
    schema,
    plugins: [
        // Proper shutdown for the HTTP server.
        ApolloServerPluginDrainHttpServer({httpServer}),

        // Proper shutdown for the WebSocket server.
        {
            async serverWillStart() {
                return {
                    async drainServer() {
                        await serverCleanup.dispose();
                    },
                };
            },
        },
    ],
});

expressServer.use(cookieParser());
expressServer.use(bodyParser.json({limit: "50mb"}));
expressServer.use(express.static(path.join(__dirname, "client/build")));

expressServer.get("*", (req, res) => {
    res.sendFile(path.join(__dirname, "client/build/index.html"));
});

httpServer.listen(port, () => {
    console.log(`Server is now running on http://localhost:${port}/graphql`);
});

await apolloServer.start();
apolloServer.applyMiddleware({app: expressServer});
