module.exports = {
	printWidth: 120,
	useTabs: true,
	importOrder: [
		"react-app-polyfill/(.*)$",
		"<THIRD_PARTY_MODULES>",
		"^[./]",
		"^pages/(.*)$|^reports/(.*)$",
		"^constants/(.*)$"
	],
	importOrderCaseInsensitive: true,
	importOrderSeparation: true,
	importOrderSortSpecifiers: true,
};
