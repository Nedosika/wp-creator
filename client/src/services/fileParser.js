import XMLParser from "react-xml-parser";

const fileParser = (file) =>
	new Promise((resolve, reject) => {
		const fileReader = new FileReader();

		fileReader.onload = (event) => {
			switch (file.type) {
				case "application/json":
					// return resolve(
					//     JSON.parse(event.target.result.toString())
					//         .map(({urls}) => Object.entries(urls).map(([_, value]) => value)[0])
					//         .filter(({mimetype}) => mimetype === 'text/html')
					//         .map(({url}) => url)
					// );
					return resolve(
						JSON.parse(event.target.result.toString()).map(({ file_url: url, timestamp }) => ({ url, timestamp })),
					);
				case "text/xml":
					return resolve(
						new XMLParser()
							.parseFromString(event.target.result.toString())
							.getElementsByTagName("loc")
							.map((loc) => loc.value),
					);
				default:
					reject("Only xml or json file supported!");
			}
		};

		fileReader.onerror = reject;

		fileReader.readAsText(file, "UTF-8");
	});

export default fileParser;
