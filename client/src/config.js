const CONFIG = {
	uri: `http://${process.env.REACT_APP_URL}:${process.env.REACT_APP_PORT}/graphql`,
	wsURL: `ws://${process.env.REACT_APP_URL}:${process.env.REACT_APP_PORT}/graphql`,
	webArchivePrefix: process.env.REACT_APP_WEB_ARCHIVE_PREFIX || "http://web.archive.org/web",
};
export default CONFIG;
