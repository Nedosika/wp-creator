import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Resizable } from "react-resizable";
import { Button, FloatButton, Popconfirm, Progress, Space, Table } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import { useMutation, useQuery, useSubscription } from "@apollo/client";

import "./Dashboard.css";
import { DIALOGS, useDialog } from "contexts/Dialog/DialogProvider";
import { GET_TASKS } from "apollo/queries";
import { REMOVE_TASKS } from "apollo/mutations";
import { TASKS_SUBSCRIPTION } from "apollo/subscriptions";

const ResizableTitle = (props) => {
	const { onResize, width, ...restProps } = props;

	if (!width) {
		return <th {...restProps} />;
	}

	return (
		<Resizable
			width={width}
			height={0}
			handle={
				<span
					className="react-resizable-handle"
					onClick={(e) => {
						e.stopPropagation();
					}}
				/>
			}
			onResize={onResize}
			draggableOpts={{
				enableUserSelectHack: false,
			}}
		>
			<th {...restProps} />
		</Resizable>
	);
};

const initialColumns = [
	{
		title: "ID",
		dataIndex: "id",
		width: 100,
		sorter: (a, b) => a.id - b.id,
		filterSearch: true,
		onFilter: (value, record) => record.name.indexOf(value) === 0,
	},
	{
		title: "Date",
		dataIndex: "date",
		width: 100,
	},
	{
		title: "Name",
		dataIndex: "name",
		width: 100,
		render: (_, record) => <Space size="middle">{record.name}</Space>,
	},
	{
		title: "Status",
		dataIndex: "status",
		width: 100,
		sorter: (a, b) => a.amount - b.amount,
		filterSearch: true,
		onFilter: (value, record) => record.name.indexOf(value) === 0,
	},
	{
		title: "Progress",
		dataIndex: "progress",
		width: 100,
		render: (props) => <Progress percent={props} size="small" />,
	},
	{
		title: "End Date",
		dataIndex: "end_date",
		width: 100,
	},
];
const Dashboard = () => {
	const { openDialog } = useDialog();
	const { loading, data = {}, refetch } = useQuery(GET_TASKS);
	const [removeTasks] = useMutation(REMOVE_TASKS, { refetchQueries: [GET_TASKS] });
	const { data: subscriptionData } = useSubscription(TASKS_SUBSCRIPTION);
	const [selectedRowKeys, setSelectedRowKeys] = useState([]);
	const [columns, setColumns] = useState(initialColumns);

	const onSelectChange = useCallback(
		(newSelectedRowKeys, _, { type }) => {
			if (type === "single") {
				return setSelectedRowKeys(newSelectedRowKeys);
			}

			selectedRowKeys.length ? setSelectedRowKeys([]) : setSelectedRowKeys(data.tasks.map(({ id }) => id));
		},
		[data.tasks, selectedRowKeys.length]
	);

	const handleResize = useCallback(
		(index) =>
			(_, { size }) => {
				const newColumns = [...columns];
				newColumns[index] = {
					...newColumns[index],
					width: size.width,
				};
				setColumns(newColumns);
			},
		[columns]
	);

	const handleOpenDialog = useCallback(() => openDialog({ dialog: DIALOGS.urls }), [openDialog]);

	const rowSelection = useMemo(
		() => ({
			selectedRowKeys,
			onChange: onSelectChange,
		}),
		[onSelectChange, selectedRowKeys]
	);

	const mergeColumns = useMemo(
		() =>
			columns.map((col, index) => ({
				...col,
				onHeaderCell: (column) => ({
					width: column.width,
					onResize: handleResize(index),
				}),
			})),
		[columns, handleResize]
	);

	const hasSelected = useMemo(() => !!selectedRowKeys.length, [selectedRowKeys.length]);

	useEffect(() => {
		subscriptionData && refetch();
	}, [subscriptionData, refetch]);

	if (loading) return null;

	return (
		<>
			<div style={{ marginBottom: 10 }}>
				<Space>
					<Popconfirm
						title="Delete the task"
						description="Are you sure to delete this task(s)?"
						onConfirm={() => removeTasks({ variables: { ids: selectedRowKeys } }).then(() => setSelectedRowKeys([]))}
						okText="Yes"
						cancelText="No"
						disabled={!hasSelected}
					>
						<Button danger disabled={!hasSelected}>
							Delete
						</Button>
					</Popconfirm>
					<Button onClick={handleOpenDialog}>Add</Button>
				</Space>

				<span style={{ marginLeft: 8 }}>{hasSelected ? `Selected ${selectedRowKeys.length} items` : ""}</span>
			</div>
			<Table
				data-testid="dashboard_table"
				bordered
				components={{ header: { cell: ResizableTitle } }}
				columns={mergeColumns}
				dataSource={data.tasks}
				rowSelection={rowSelection}
				rowKey="id"
			/>
			<FloatButton
				data-testid="add_float_button"
				onClick={handleOpenDialog}
				shape="circle"
				icon={<PlusOutlined />}
				size="large"
			/>
		</>
	);
};

export default Dashboard;
