import Dashboard from "./Dashboard";
import { fireEvent, render, screen } from "@testing-library/react";
import TestDialogProvider from "contexts/Dialog/TestDialogProvider";
import { MockedProvider as TestApolloProvider } from "@apollo/client/testing";
import { TASKS_SUBSCRIPTION } from "apollo/subscriptions";
import { GET_TASKS } from "apollo/queries";
import CreateTaskDialog from "dialogs/CreateTaskDialog";

const mocks = [
	{
		request: {
			query: TASKS_SUBSCRIPTION,
		},
	},
	{
		request: {
			query: GET_TASKS,
		},
		result: { data: { tasks: [] } },
	},
];

const openDialog = jest.fn();
const TestComponent = ({ dialogContext = {} }) => (
	<TestApolloProvider mocks={mocks}>
		<TestDialogProvider context={{ openDialog, ...dialogContext }}>
			<Dashboard />
		</TestDialogProvider>
	</TestApolloProvider>
);

describe("Dashboard Component", () => {
	test("render DashboardComponent", async () => {
		render(<TestComponent />);

		const dashboardTable = await screen.findByTestId("dashboard_table");
		const deleteButton = await screen.findByRole("button", { name: "Delete" });
		const addButton = await screen.findByRole("button", { name: "Add" });
		const addFloatButton = await screen.findByTestId("add_float_button");

		expect(dashboardTable).toBeInTheDocument();

		expect(deleteButton).toBeInTheDocument();
		expect(deleteButton).toBeDisabled();

		expect(addButton).toBeInTheDocument();
		expect(addButton).toBeEnabled();

		expect(addFloatButton).toBeInTheDocument();
	});

	test("should open add task dialog when clicked on add button", async () => {
		render(<TestComponent />);

		const addButton = await screen.findByRole("button", { name: "Add" });

		fireEvent.click(addButton);

		expect(openDialog).toBeCalledWith({ dialog: CreateTaskDialog });
	});

	test("should open add task dialog when clicked on add float button", async () => {
		render(<TestComponent />);

		const addFloatButton = await screen.findByTestId("add_float_button");

		fireEvent.click(addFloatButton);

		expect(openDialog).toBeCalledWith({ dialog: CreateTaskDialog });
	});
});
