import React from "react";
import { Input, Space } from "antd";
import { EyeInvisibleOutlined, EyeTwoTone, UserOutlined } from "@ant-design/icons";
import { SETTINGS, useSettings } from "contexts/Settings";

const WordpressSettings = () => {
	const {
		settings: {
			[SETTINGS.endpoint]: endpoint,
			[SETTINGS.endpointSuffix]: suffix,
			[SETTINGS.username]: username,
			[SETTINGS.password]: password,
		},
		updateSettings,
	} = useSettings();

	const handleChange =
		(key) =>
		({ target: { value } }) =>
			updateSettings({ [key]: value });

	return (
		<Space direction="vertical">
			<Space.Compact>
				<Input addonBefore="endpoint" value={endpoint} onChange={handleChange(SETTINGS.endpoint)} />
				<Input value={suffix} onChange={handleChange(SETTINGS.endpointSuffix)} style={{ width: "100px" }} />
			</Space.Compact>
			<Input
				data-testid="wp_login"
				placeholder="wp login"
				prefix={<UserOutlined />}
				value={username}
				onChange={handleChange(SETTINGS.username)}
			/>
			<Input.Password
				addonBefore="password"
				placeholder="input password"
				iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
				value={password}
				onChange={handleChange(SETTINGS.password)}
			/>
		</Space>
	);
};

export default WordpressSettings;
