import TestSettingsProvider from "contexts/Settings/TestSettingsProvider";
import WordpressSettings from "./WordpressSettings";
import { fireEvent, render, screen } from "@testing-library/react";

const updateSettings = jest.fn();

const TestComponent = () => (
	<TestSettingsProvider context={{ updateSettings }}>
		<WordpressSettings />
	</TestSettingsProvider>
);

describe("WordpressSettings component", () => {
	test("render component", () => {
		render(<TestComponent />);

		expect(screen.getByText("endpoint")).toBeInTheDocument();
		expect(screen.getByText("password")).toBeInTheDocument();
		expect(screen.getByTestId("wp_login")).toBeInTheDocument();
	});

	test("should call updateSettings when change data", () => {
		render(<TestComponent />);

		fireEvent.change(screen.getByTestId("wp_login"), { target: { value: "New Value" } });

		expect(updateSettings).toBeCalledWith({ username: "New Value" });
	});
});
