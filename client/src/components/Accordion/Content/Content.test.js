import Content from "./Content";
import { fireEvent, render, screen } from "@testing-library/react";
import TestSettingsProvider from "contexts/Settings/TestSettingsProvider";

const updateSettings = jest.fn();
const TestComponent = () => (
	<TestSettingsProvider context={{ updateSettings }}>
		<Content />
	</TestSettingsProvider>
);
describe("Content component", () => {
	test("render Content component", () => {
		render(<TestComponent />);

		expect(screen.getByTestId("content_component")).toBeInTheDocument();
		expect(screen.getByText("Header selector")).toBeInTheDocument();
		expect(screen.getByText("Date selector")).toBeInTheDocument();
		expect(screen.getByText("Date parser")).toBeInTheDocument();
		expect(screen.getByText("Content Selector")).toBeInTheDocument();

		expect(screen.getByText("Add replacer")).toBeInTheDocument();
	});

	test("should call updateSettings when Add Replacer button clicked", () => {
		render(<TestComponent />);

		fireEvent.click(screen.getByText("Add replacer"));

		expect(updateSettings).toBeCalledWith({ contentReplacers: [""] });
	});
});
