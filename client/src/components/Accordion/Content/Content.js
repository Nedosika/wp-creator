import React from "react";
import { Input, Space, Button } from "antd";
import { MinusCircleOutlined, PlusOutlined } from "@ant-design/icons";
import styles from "./Content.module.css";
import { SETTINGS, useSettings } from "contexts/Settings";

const Content = () => {
	const {
		settings: {
			[SETTINGS.headerSelector]: headerSelector,
			[SETTINGS.contentSelector]: contentSelector,
			[SETTINGS.contentReplacers]: contentReplacers = [],
			[SETTINGS.dateSelector]: dateSelector,
			[SETTINGS.dateParser]: dateParser,
			[SETTINGS.dateLocale]: dateLocale,
		},
		updateSettings,
	} = useSettings();

	const handleChange =
		(key) =>
		({ target: { value } }) =>
			updateSettings({ [key]: value });

	const handleChangeReplacers =
		(index) =>
		({ target: { value } }) => {
			updateSettings({
				[SETTINGS.contentReplacers]: [...contentReplacers.slice(0, index), value, ...contentReplacers.slice(index + 1)],
			});
		};

	const handleAddReplacer = () => {
		updateSettings({ [SETTINGS.contentReplacers]: [...contentReplacers, ""] });
	};

	const handleRemoveReplacer = (index) => () => {
		updateSettings({
			[SETTINGS.contentReplacers]: [...contentReplacers.slice(0, index), ...contentReplacers.slice(index + 1)],
		});
	};

	return (
		<Space direction="vertical" data-testid={"content_component"}>
			<Input addonBefore="Header selector" value={headerSelector} onChange={handleChange(SETTINGS.headerSelector)} />
			<Input addonBefore="Date selector" value={dateSelector} onChange={handleChange(SETTINGS.dateSelector)} />
			<Space.Compact>
				<Input addonBefore="Date parser" value={dateParser} onChange={handleChange(SETTINGS.dateParser)} />
				<Input value={dateLocale} onChange={handleChange(SETTINGS.dateLocale)} style={{ width: "100px" }} />
			</Space.Compact>
			<Input addonBefore="Content Selector" value={contentSelector} onChange={handleChange(SETTINGS.contentSelector)} />
			{contentReplacers.map((value, index) => (
				<div key={value}>
					<Input
						addonBefore="Replacer"
						value={value}
						onChange={handleChangeReplacers(index)}
						style={{ width: "87%" }}
					/>
					<MinusCircleOutlined className={styles.dynamicDeleteButton} onClick={handleRemoveReplacer(index)} />
				</div>
			))}
			<Button type="dashed" style={{ width: "100%" }} icon={<PlusOutlined />} onClick={handleAddReplacer}>
				Add replacer
			</Button>
		</Space>
	);
};

export default Content;
