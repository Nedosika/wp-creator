import React from "react";
import { Navigate, Outlet, useLocation } from "react-router-dom";

const ProtectedRoute = ({ children, allowed, redirect }) => {
	let location = useLocation();

	if (!allowed) {
		return <Navigate to={redirect} state={{ from: location }} replace />;
	}

	return children ? children : <Outlet />;
};

export default ProtectedRoute;
