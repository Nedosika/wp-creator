import { Collapse, Modal } from "antd";
import Content from "components/Accordion/Content";

import React from "react";
import { useDialog } from "contexts/Dialog/DialogProvider";
import WordpressSettings from "components/Accordion/WordpressSettings";

const { Panel } = Collapse;

const SettingsDialog = () => {
	const { closeDialog } = useDialog();

	return (
		<Modal data-testid="settings_modal_dialog" title="Settings" centered open onOk={closeDialog} onCancel={closeDialog}>
			<Collapse accordion defaultActiveKey={1}>
				<Panel header="Content" key="1">
					<Content />
				</Panel>
				<Panel header="Wordpress settings" key="3">
					<WordpressSettings />
				</Panel>
			</Collapse>
		</Modal>
	);
};
export default SettingsDialog;
