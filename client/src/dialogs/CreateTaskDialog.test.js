import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import CreateTaskDialog from "./CreateTaskDialog";
import TestDialogProvider from "contexts/Dialog/TestDialogProvider";
import TestSettingsProvider from "contexts/Settings/TestSettingsProvider";
import { MockedProvider as TestApolloProvider } from "@apollo/client/testing";
import { CREATE_TASK } from "../apollo/mutations";
import { GET_TASKS } from "../apollo/queries";
import { SETTINGS } from "../contexts/Settings";

const closeDialog = jest.fn();
const openDialog = jest.fn();
const updateSettings = jest.fn();

const mockedData = {
	contentReplacers: ['class="[^"]*"', 'style="[^"]*"', 'id="[^"]*"'],
	contentSelector: ".post_content",
	dateLocale: "ru",
	dateParser: "MMM DD, YYYY",
	dateSelector: ".post-date",
	endpoint: "https://0.userpk.ru/wp-json",
	endpointSuffix: "endpointSuffix",
	headerSelector: "h1",
	password: "nFFy g33d y4El m0w2 1nCp QEWJ",
	timeout: 1000,
	urls: [{}],
	username: "admin55",
};

const mocks = [
	{
		request: {
			query: CREATE_TASK,
			variables: {
				data: {
					contentReplacers: mockedData.contentReplacers,
					contentSelector: mockedData.contentSelector,
					dateLocale: mockedData.dateLocale,
					dateParser: mockedData.dateParser,
					dateSelector: mockedData.dateSelector,
					endpoint: `${mockedData.endpoint}/${mockedData.endpointSuffix}`,
					headerSelector: mockedData.headerSelector,
					name: mockedData.endpoint,
					password: mockedData.password,
					timeout: mockedData.timeout,
					urls: mockedData.urls,
					username: mockedData.username,
				},
			},
		},
		result: { data: { createTask: {} } },
	},
	{
		request: {
			query: GET_TASKS,
		},
		result: { data: { tasks: [] } },
	},
];

const TestComponent = ({ taskContext = {}, dialogContext = {} }) => (
	<TestApolloProvider mocks={mocks}>
		<TestSettingsProvider context={taskContext}>
			<TestDialogProvider context={{ closeDialog, openDialog, ...dialogContext }}>
				<CreateTaskDialog />
			</TestDialogProvider>
		</TestSettingsProvider>
	</TestApolloProvider>
);

describe("CreateTaskDialog Component", () => {
	test("should render title, file upload button and table", () => {
		render(<TestComponent />);

		expect(screen.getByText("Create task")).toBeInTheDocument();
		expect(screen.getByText("File")).toBeInTheDocument();
		expect(screen.getByText("URL")).toBeInTheDocument();
		expect(screen.getByText("Date")).toBeInTheDocument();
		expect(screen.getByText("OK")).toBeInTheDocument();
		expect(screen.getByText("Cancel")).toBeInTheDocument();
	});

	test("should close dialog when click to cancel button", () => {
		render(<TestComponent />);

		fireEvent.click(screen.getByText("Cancel"));

		expect(closeDialog).toBeCalled();
	});

	test("should disable OK button when urls are not available", () => {
		render(<TestComponent />);

		const okButton = screen.getByRole("button", { name: "OK" });

		expect(okButton).toBeInTheDocument();
		expect(okButton).toBeDisabled();
	});

	test("should enable OK button when urls are available", () => {
		render(<TestComponent taskContext={{ settings: mockedData }} />);

		const okButton = screen.getByRole("button", { name: "OK" });

		expect(okButton).toBeInTheDocument();
		expect(okButton).toBeEnabled();
	});

	test("should call closeDialog mutation when clicked button OK", () => {
		render(<TestComponent taskContext={{ settings: mockedData }} />);

		const okButton = screen.getByRole("button", { name: "OK" });

		expect(okButton).toBeInTheDocument();
		expect(okButton).toBeEnabled();

		fireEvent.click(okButton);

		waitFor(() => expect(closeDialog).toBeCalled());
	});
});
