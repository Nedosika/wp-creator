import { Modal } from "antd";
import { ObjectView } from "react-object-view";

import React from "react";
import { useDialog } from "contexts/Dialog/DialogProvider";

const InfoDialog = (data) => {
	const { closeDialog } = useDialog();

	return (
		<Modal title="Urls information" centered open onOk={closeDialog} onCancel={closeDialog} width="100%">
			<ObjectView data={data} />
		</Modal>
	);
};

export default InfoDialog;
