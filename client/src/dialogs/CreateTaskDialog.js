import React, { useCallback, useMemo, useState } from "react";
import { Button, message, Modal, Space, Table, Upload } from "antd";
import { SettingOutlined, SyncOutlined, UploadOutlined } from "@ant-design/icons";
import fileParser from "services/fileParser";
import { DIALOGS, useDialog } from "contexts/Dialog/DialogProvider";
import moment from "moment";
import CONFIG from "../config";
import { SETTINGS, useSettings } from "contexts/Settings";
import { useLazyQuery, useMutation } from "@apollo/client";
import { CREATE_TASK } from "apollo/mutations";
import { CHECK } from "apollo/queries";

const dateRender = (date) => moment(date).format("YYYY/MM/DD");

const URL_FILTERS = [
	/\/tag\//g,
	/\.(jpg|png|gif)\b/g,
	/\/sitemap.xml/g,
	/\/sitemap.rss/g,
	/\/wp-includes\//g,
	/\/wp-content\//g,
	/\/wp-json\//g,
	/\/comment-subscriptions\//g,
	/\/sajt-hydra\//g,
	/\/ssylka-gidry\//g,
	/\/sajt-gidra-2\//g,
];

const CreateTaskDialog = () => {
	const [checkUrl, { loading, error }] = useLazyQuery(CHECK);
	const [checkError, setCheckError] = useState(null);
	const { openDialog, closeDialog } = useDialog();
	const {
		settings: {
			[SETTINGS.endpoint]: endpoint,
			[SETTINGS.contentReplacers]: contentReplacers,
			[SETTINGS.contentSelector]: contentSelector,
			[SETTINGS.dateLocale]: dateLocale,
			[SETTINGS.dateParser]: dateParser,
			[SETTINGS.dateSelector]: dateSelector,
			[SETTINGS.endpointSuffix]: endpointSuffix,
			[SETTINGS.headerSelector]: headerSelector,
			[SETTINGS.password]: password,
			[SETTINGS.timeout]: timeout,
			[SETTINGS.username]: username,
			[SETTINGS.urls]: urls = [],
		},
		updateSettings,
	} = useSettings();

	const [loadedUrls, setLoadedUrls] = useState([]);
	const [createTask] = useMutation(CREATE_TASK);

	const handleCreateTask = useCallback(
		() =>
			createTask({
				variables: {
					data: {
						contentReplacers,
						contentSelector,
						dateLocale,
						dateParser,
						dateSelector,
						endpoint: `${endpoint}/${endpointSuffix}`,
						headerSelector,
						name: endpoint,
						password,
						timeout,
						urls,
						username,
					},
				},
			}).then(closeDialog),
		[
			closeDialog,
			contentReplacers,
			contentSelector,
			createTask,
			dateLocale,
			dateParser,
			dateSelector,
			endpoint,
			endpointSuffix,
			headerSelector,
			password,
			timeout,
			urls,
			username,
		],
	);
	const onSelectRow = useCallback(
		(newSelectedRowKeys) =>
			updateSettings({ [SETTINGS.urls]: loadedUrls.filter(({ url }) => newSelectedRowKeys.includes(url)) }),
		[loadedUrls, updateSettings],
	);

	const rowSelection = {
		selectedRowKeys: urls.map(({ url }) => url),
		onChange: onSelectRow,
		selections: [Table.SELECTION_ALL, Table.SELECTION_NONE],
	};

	const handleCheck = (checkedUrl) => {
		setCheckError(null);
		const url = urls.find(({ url }) => url === checkedUrl);

		url
			? checkUrl({
					variables: {
						data: { url, headerSelector, contentSelector, contentReplacers, dateSelector, dateParser, dateLocale },
					},
			  })
					.then(({ data: { check } }) => openDialog({ dialog: DIALOGS.info, props: check }))
					.catch((error) => setCheckError(error.message))
			: setCheckError("url not found");
	};

	const columns = [
		{
			title: "URL",
			dataIndex: "url",
			//filters: [],
			//onFilter: (value, record) => record.name.indexOf(value) === 0,
			//sorter: (a, b) => a.url > b.url,
			render: (url) => (
				<a href={`${CONFIG.webArchivePrefix}/${url}`} target="_blank">
					{url}
				</a>
			),
		},
		{
			title: "Date",
			dataIndex: "date",
			render: dateRender,
		},
		{
			title: "Action",
			key: "operation",
			fixed: "right",
			width: 100,
			render: ({ url }) => (
				<Button
					loading={loading}
					danger={!!error || !!checkError}
					icon={<SyncOutlined />}
					onClick={() => handleCheck(url)}
				>
					Check
				</Button>
			),
		},
	];

	const handleRequest = ({ file, onSuccess }) =>
		fileParser(file)
			.then((urls) => {
				const filteredUrls = urls
					.filter(({ url }) => URL_FILTERS.every((filter) => url.search(filter) < 0))
					.map(({ url, timestamp }) => ({
						url,
						webArchiveUrl: `${CONFIG.webArchivePrefix}/${timestamp}`,
						date: moment(timestamp, "YYYYMMDDHHmmss").toDate().toString(),
					}));
				setLoadedUrls(filteredUrls);
				updateSettings({ [SETTINGS.urls]: filteredUrls });

				message.info(`Loaded ${filteredUrls.length} url(s)`);

				onSuccess();
			})
			.catch((error) => message.error(error));

	const handleOpenTaskDialog = () => {
		openDialog({ dialog: DIALOGS.task });
	};

	return (
		<Modal
			title="Create task"
			centered
			open
			width="100%"
			onOk={handleCreateTask}
			onCancel={closeDialog}
			okButtonProps={{ disabled: !urls.length }}
		>
			<Space style={{ marginBottom: 16, alignItems: "baseline" }}>
				<Upload name="file" customRequest={handleRequest} maxCount={1}>
					<Button icon={<UploadOutlined />}>File</Button>
				</Upload>
				<Button icon={<SettingOutlined />} onClick={handleOpenTaskDialog}>
					Settings
				</Button>
			</Space>
			<Table columns={columns} dataSource={loadedUrls} rowSelection={rowSelection} rowKey="url" />
		</Modal>
	);
};

export default CreateTaskDialog;
