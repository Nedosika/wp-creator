import TestDialogProvider from "contexts/Dialog/TestDialogProvider";
import InfoDialog from "./InfoDialog";
import { fireEvent, render, screen } from "@testing-library/react";

const closeDialog = jest.fn();

const TestComponent = () => (
	<TestDialogProvider context={{ closeDialog }}>
		<InfoDialog />
	</TestDialogProvider>
);

describe("InfoDialog Component", () => {
	test("render dialog with content", () => {
		render(<TestComponent />);

		expect(screen.getByText("Urls information")).toBeInTheDocument();
		expect(screen.getByText("OK")).toBeInTheDocument();
	});

	test("should close dialog when click to OK button", () => {
		render(<TestComponent />);

		fireEvent.click(screen.getByText("OK"));

		expect(closeDialog).toBeCalled();
	});
});
