import SettingsDialog from "./SettingsDialog";
import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import { MockedProvider as TestApolloProvider } from "@apollo/client/testing";
import TestDialogProvider from "contexts/Dialog/TestDialogProvider";
import TestSettingsProvider from "contexts/Settings/TestSettingsProvider";
import { GET_TASKS } from "apollo/queries";
import { CREATE_TASK } from "apollo/mutations";

const closeDialog = jest.fn();

const mocks = [
	{
		request: {
			query: CREATE_TASK,
			variables: {
				data: {
					name: "https://0.userpk.ru",
					isAddCategories: true,
					onlyHtml: true,
					urls: [],
					headerSelector: "h1",
					contentSelector: ".post_content",
					contentReplacers: ['class="[^"]*"', 'style="[^"]*"', 'id="[^"]*"'],
					dateParser: "MMM DD, YYYY",
					dateLocale: "ru",
					dateSelector: ".post-date",
					isStrongSearch: false,
					sortBy: "title",
					order: "asc",
					username: "admin55",
					password: "nFFy g33d y4El m0w2 1nCp QEWJ",
					endpoint: "https://0.userpk.ru/wp-json",
					timeout: 1000,
				},
			},
		},
		result: { data: { createTask: {} } },
	},
	{
		request: {
			query: GET_TASKS,
		},
		result: { data: { tasks: [] } },
	},
];

const TestComponent = ({ context }) => (
	<TestApolloProvider mocks={mocks}>
		<TestSettingsProvider>
			<TestDialogProvider context={{ closeDialog, ...context }}>
				<SettingsDialog />
			</TestDialogProvider>
		</TestSettingsProvider>
	</TestApolloProvider>
);

describe("SettingsDialog Component", () => {
	test("render SettingsDialog", () => {
		render(<TestComponent />);

		const title = screen.getByText("Settings");
		const okButton = screen.getByRole("button", { name: "OK" });
		const cancelButton = screen.getByRole("button", { name: "Cancel" });
		const dialog = screen.getByTestId("settings_modal_dialog");

		expect(title).toBeInTheDocument();
		expect(okButton).toBeInTheDocument();
		expect(cancelButton).toBeInTheDocument();
		expect(dialog).toBeInTheDocument();
	});

	test("should call closeDialog when click on cancel button", () => {
		render(<TestComponent />);

		const cancelButton = screen.getByRole("button", { name: "Cancel" });

		fireEvent.click(cancelButton);

		expect(closeDialog).toBeCalled();
	});

	test("should call closeAllDialogs when click on OK button", async () => {
		render(<TestComponent />);

		const okButton = screen.getByRole("button", { name: "OK" });

		fireEvent.click(okButton);

		await waitFor(() => expect(closeDialog).toBeCalled());
	});
});
