import AppRouter from "components/AppRouter/AppRouter";
import DialogProvider from "contexts/Dialog/DialogProvider";
import { ApolloProvider } from "@apollo/client";
import { apolloClient } from "apollo/client";
import ErrorBoundary from "components/ErrorBoundary";
import SettingsProvider from "./contexts/Settings";

const App = () => (
	<ErrorBoundary>
		<ApolloProvider client={apolloClient}>
			<SettingsProvider>
				<DialogProvider>
					<AppRouter />
				</DialogProvider>
			</SettingsProvider>
		</ApolloProvider>
	</ErrorBoundary>
);
export default App;
