import { ApolloClient, ApolloLink, InMemoryCache } from "@apollo/client";
import { errorLink, httpLink, wsLink } from "./links";
import { split } from "@apollo/client";
import { getMainDefinition } from "@apollo/client/utilities";

const splitLink = split(
	({ query }) => {
		const definition = getMainDefinition(query);
		return definition.kind === "OperationDefinition" && definition.operation === "subscription";
	},
	wsLink,
	httpLink
);

export const apolloClient = new ApolloClient({
	connectToDevTools: true,
	link: ApolloLink.from([errorLink, splitLink]),
	cache: new InMemoryCache(),
});
