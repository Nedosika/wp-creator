import { gql } from "@apollo/client";

export const REFRESH_TOKEN = gql`
	query refreshToken {
		refreshToken {
			tokens {
				accessToken
				refreshToken
			}
		}
	}
`;

export const GET_TASKS = gql`
	query {
		tasks {
			id
			name
			progress
			status
		}
	}
`;

export const GET_TASK = gql`
	query Query($id: ID) {
		task(id: $id) {
			id
			urls
		}
	}
`;

export const CHECK = gql`
	query Check($data: CheckInput) {
		check(data: $data) {
			slug
			slugCategories
			header
			title
			date
			content
			description
		}
	}
`;
