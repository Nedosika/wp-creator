import { gql } from "@apollo/client";

export const TASKS_SUBSCRIPTION = gql`
	subscription {
		tasksUpdated {
			id
			progress
		}
	}
`;
