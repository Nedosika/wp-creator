import { renderHook } from "@testing-library/react";
import SettingsProvider, { useSettings } from "./SettingsProvider";

describe("useTask hook", () => {
	test("should error when not used with a provider", () => {
		jest.spyOn(console, "error").mockImplementation(() => {});
		try {
			renderHook(() => useSettings());
		} catch (error) {
			expect(error).toEqual(Error("useSettings must be used within a SettingsProvider!"));
		} finally {
			console.error.mockRestore();
		}
	});

	test("returns default elements and export functions", () => {
		const { result } = renderHook(() => useSettings(), { wrapper: SettingsProvider });

		expect(typeof result.current.settings).toBe("object");
		expect(typeof result.current.updateSettings).toBe("function");
	});
});
