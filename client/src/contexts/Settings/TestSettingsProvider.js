import { SettingsContext } from "./SettingsProvider";
const TestSettingsProvider = ({ children, context = {} }) => (
	<SettingsContext.Provider value={{ settings: {}, updateSettings: jest.fn, ...context }}>
		{children}
	</SettingsContext.Provider>
);

export default TestSettingsProvider;
