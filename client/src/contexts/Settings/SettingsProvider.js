import { createContext, useContext, useState } from "react";

export const SETTINGS = {
	endpoint: "endpoint",
	endpointSuffix: "endpointSuffix",
	dateSelector: "dateSelector",
	dateParser: "dateParser",
	dateLocale: "dateLocale",
	username: "username",
	password: "password",
	headerSelector: "headerSelector",
	contentSelector: "contentSelector",
	contentReplacers: "contentReplacers",
	isLoading: "isLoading",
	progress: "progress",
	timeout: "timeout",
	urls: "urls",
};

export const SettingsContext = createContext(null);

const initialState = {
	[SETTINGS.endpoint]: "https://0.userpk.ru",
	[SETTINGS.endpointSuffix]: "wp-json",
	[SETTINGS.username]: "admin55",
	[SETTINGS.password]: "nFFy g33d y4El m0w2 1nCp QEWJ",
	[SETTINGS.headerSelector]: "h1",
	[SETTINGS.contentSelector]: ".post_content",
	[SETTINGS.contentReplacers]: ['class="[^"]*"', 'style="[^"]*"', 'id="[^"]*"'],
	[SETTINGS.dateSelector]: ".post-date",
	[SETTINGS.dateParser]: "MMM DD, YYYY",
	[SETTINGS.dateLocale]: "ru",
	[SETTINGS.progress]: 0,
	[SETTINGS.timeout]: 1000,
	[SETTINGS.urls]: [],
	[SETTINGS.isLoading]: false,
};

const SettingsProvider = ({ children }) => {
	const [settings, setSettings] = useState(initialState);

	const updateSettings = (value) => setSettings((prevState) => ({ ...prevState, ...value }));

	return <SettingsContext.Provider value={{ settings, updateSettings }}>{children}</SettingsContext.Provider>;
};

export const useSettings = () => {
	const context = useContext(SettingsContext);

	if (!context) {
		throw new Error("useSettings must be used within a SettingsProvider!");
	}

	return context;
};

export default SettingsProvider;
