import { createContext, createElement, useContext, useState } from "react";
import SettingsDialog from "dialogs/SettingsDialog";
import InfoDialog from "dialogs/InfoDialog";
import DeleteDialog from "dialogs/DeleteDialog";
import CreateTaskDialog from "dialogs/CreateTaskDialog";

export const DIALOGS = {
	task: SettingsDialog,
	info: InfoDialog,
	remove: DeleteDialog,
	urls: CreateTaskDialog,
};

export const DialogContext = createContext(null);

export const useDialog = () => {
	const context = useContext(DialogContext);

	if (!context) {
		throw new Error("useDialog must be used within a DialogProvider!");
	}

	return context;
};

export const DialogProvider = ({ children }) => {
	const [dialogs, setDialogs] = useState([]);

	const openDialog = ({ dialog, props = {} }) => {
		setDialogs((prevState) => [...prevState, { dialog, props }]);
	};

	const closeDialog = () => {
		setDialogs((prevState) => prevState.slice(0, dialogs.length - 1));
	};

	const closeAllDialogs = () => {
		setDialogs([]);
	};

	return (
		<DialogContext.Provider
			value={{
				openDialog,
				closeDialog,
				closeAllDialogs,
			}}
		>
			{children}
			{dialogs.map(({ dialog, props }, index) => createElement(dialog, { ...props, key: index }))}
		</DialogContext.Provider>
	);
};

export default DialogProvider;
