import { DialogContext } from "./DialogProvider";

const TestDialogProvider = ({ children, context }) => {
	const value = {
		openDialog: jest.fn(),
		closeDialog: jest.fn(),
		closeAllDialogs: jest.fn(),
		...context,
	};

	return <DialogContext.Provider value={value}>{children}</DialogContext.Provider>;
};

export default TestDialogProvider;
