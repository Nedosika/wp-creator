import { renderHook } from "@testing-library/react";

import { DialogProvider, useDialog } from "./DialogProvider";

describe("useDialog hook", () => {
	test("should error when not used with a provider", () => {
		jest.spyOn(console, "error").mockImplementation(() => {});

		try {
			renderHook(() => useDialog());
		} catch (error) {
			expect(error).toEqual(Error("useDialog must be used within a DialogProvider!"));
		} finally {
			console.error.mockRestore();
		}
	});

	test("returns default elements and export functions", () => {
		const { result } = renderHook(() => useDialog(), { wrapper: DialogProvider });

		expect(typeof result.current.openDialog).toBe("function");
		expect(typeof result.current.closeDialog).toBe("function");
	});
});
